
Pod::Spec.new do |s|
  s.name         = "ArgumentKit"
  s.version      = "1.1.0"
  s.summary      = "A visual way to manage arguments to your iOS application"
  s.author        = { "Zakk Hoyt" => "vaporwarewolf@gmail.com" }
  s.homepage      = "https://gitlab.com/zakkhoyt/ArgumentKit"
  s.platforms = { :ios => 10.0
                }
  s.license = { :type => 'MIT',
                :text =>  <<-LICENSE
                  Copyright 2017. Zakk hoyt.
                          LICENSE
              }
  s.source       = { :git => 'https://gitlab.com/zakkhoyt/ArgumentKit.git',
                    :tag =>  "#{s.version}" }
  s.source_files  = '**/*.{swift}'
  s.resource_bundles = {
                    'ArgumentKit' => ['ArgumentKit/**/*.{xib,xcassets}']
                    }
  s.requires_arc = true
  s.ios.deployment_target = '10.0'
end
