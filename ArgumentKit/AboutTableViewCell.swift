//
//  AboutTableViewCell.swift
//  ArgumentKit
//
//  Created by Zakk Hoyt on 3/1/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

public class AboutTableViewCell: UITableViewCell {
    static public let identifier = "AboutTableViewCell"
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

}
