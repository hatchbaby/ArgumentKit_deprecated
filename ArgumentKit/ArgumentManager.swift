//
//  ArgumentManager.swift
//  ArgumentManagerExample
//
//  Created by Zakk Hoyt on 4/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import Foundation

public protocol ArgumentDefinable {
    /// The title of the argument as defined in Xcode's scheme
    var title: String { get }
    
    /// A user descrition of the argument. Displayed as a help string in the UI
    var description: String { get }
}

//public protocol EnvironmentDefinable {
//    /// The Environmental variable's name
//    var name: String { get }
//
//    /// The Environmental variable's current value
//    var value: String? { get }
//
//    /// The Environmental variable's default value
//    var defaultValue: String? { get }
//}


public extension Notification.Name {
    /// Broadcast when an arguement's enabled property changes
    /// @see ArgumentManagerArgumentKey for interpreting userInfo dictionary
    static public let ArgumentManagerArgumentsDidChange = Notification.Name("ArgumentManagerArgumentsDidChange")
}

/// Used as a userInfo key in ArgumentManagerArgumentsDidChange Notification
public let ArgumentManagerArgumentKey = "argument"

public class ArgumentManager {
    
    public static let shared = ArgumentManager()
    
    internal private(set) var argumentPresenceItems: [ArgumentPresenceItem] = []
    public var arguments: [ArgumentDefinable] {
        get {
            // Return only the enum property
            return argumentPresenceItems.compactMap{$0.e}
        }
        set {
            // embed enum in aggregate structure
            argumentPresenceItems.removeAll()
            for argumentPresenceItem in newValue {
                argumentPresenceItems.append(ArgumentPresenceItem(e: argumentPresenceItem))
            }
            setupPresence()
        }
    }
    
    
//    internal private(set) var environmentItems: [ArgumentPresenceItem] = []
//    public var environments: [EnvironmentDefinable] {
//        get {
//            // Return only the enum property
//            return argumentPresenceItems.flatMap{$0.e}
//        }
//        set {
//            // embed enum in aggregate structure
//            argumentPresenceItems.removeAll()
//            for argumentPresenceItem in newValue {
//                argumentPresenceItems.append(ArgumentPresenceItem(e: argumentPresenceItem))
//            }
//            setupPresence()
//        }
//    }
//
    
    
    
    /// Enable/Disable an argument in the .userDefault category
    public func modifyUserDefaultsArgument(_ item: ArgumentDefinable, enable: Bool) {
        guard let argument = argumentPresenceItem(from: item.title) else {
            // TODO: throw
            return
        }
        modifyUserDefaultsArgument(argument, enable: enable)
    }
    
    /// Enable/Disable an argument in the .runtime category
    public func modifyRuntimeArgument(_ item: ArgumentDefinable, enable: Bool) {
        guard let argument = argumentPresenceItem(from: item.title) else {
            // TODO: throw
            return
        }
        modifyRuntimeArgument(argument, enable: enable)
    }
    
    /// Check if an argument is enabled or not
    public func argumentEnabled(_ item: ArgumentDefinable) -> Bool {
        guard let argument = argumentPresenceItem(from: item.title) else {
            return false
        }
        return !argument.presence.isEmpty
    }
    
    /// Check if a list or arguments are enabled or not
    public func argumentsEnabled(_ items: [ArgumentDefinable]) -> Bool {
        for item in items {
            if !argumentEnabled(item) {
                return false
            }
        }
        return true
    }
    
    private func setupPresence() {
        for argument in argumentPresenceItems {
            setupPresenceFor(argumentPresenceItem: argument)
        }
    }
    
    private func setupPresenceFor(argumentPresenceItem: ArgumentPresenceItem) {
        // Xcode
        if ProcessInfo.processInfo.arguments.contains(argumentPresenceItem.title) {
            argumentPresenceItem.presence.insert(.xcode)
        } else {
            argumentPresenceItem.presence.remove(.xcode)
        }
        
        // UserDefaults
        if ArgumentManagerUserDefaults.isModeEnabled(argumentPresenceItem.title) {
            argumentPresenceItem.presence.insert(.userDefaults)
        } else {
            argumentPresenceItem.presence.remove(.userDefaults)
        }
    }
    
    internal func modifyRuntimeArgument(_ argumentPresenceItem: ArgumentPresenceItem, enable: Bool) {
        if enable {
            argumentPresenceItem.presence.insert(.runtime)
        } else {
            argumentPresenceItem.presence.remove(.runtime)
        }
        notifyWithArgument(argumentPresenceItem)
    }
    
    internal func modifyUserDefaultsArgument(_ argumentPresenceItem: ArgumentPresenceItem, enable: Bool) {
        if enable {
            ArgumentManagerUserDefaults.insertArgument(argumentPresenceItem)
        } else {
            ArgumentManagerUserDefaults.removeArgument(argumentPresenceItem)
        }
        setupPresenceFor(argumentPresenceItem: argumentPresenceItem)
        notifyWithArgument(argumentPresenceItem)
    }
    
    private func argumentPresenceItem(from string: String) -> ArgumentPresenceItem? {
        for argumentPresenceItem in argumentPresenceItems {
            if argumentPresenceItem.title == string {
                return argumentPresenceItem
            }
        }
        return nil
    }
    
    private func notifyWithArgument(_ argumentPresenceItem: ArgumentPresenceItem) {
        let userInfo: [String: Any] = [ArgumentManagerArgumentKey: argumentPresenceItem]
        NotificationCenter.default.post(name: .ArgumentManagerArgumentsDidChange, object: self, userInfo: userInfo)
    }
}


class ArgumentManagerUserDefaults {
    static let ArgumentManagerUserDefaultsKey = "ArgumentManagerUserDefaults"
    
    class func printArguments() {
        if let modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String {
            print("~~ UserDefault arguments: " + modes)
        } else {
            print("~~ UserDefault arguments: -")
        }
    }
    
    class func isModeEnabled(_ mode: String) -> Bool {
        if let modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String {
            if modes.uppercased().contains(mode.uppercased()) {
                return true
            }
        }
        return false
    }
    
    class func modesString() -> String? {
        return Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String
    }
    
    class func insertArgument(_ argumentPresenceItem: ArgumentPresenceItem) {
        guard var modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String else  {
            // Add new default
            Foundation.UserDefaults.standard.set(argumentPresenceItem.title, forKey: ArgumentManagerUserDefaultsKey)
            Foundation.UserDefaults.standard.synchronize()
            print("UserDefaults: Created new argument list with: " + argumentPresenceItem.title)
            return
        }
        
        if modes.contains(argumentPresenceItem.title) {
            // Already contains argument
            print("UserDefaults: Already contains argument: " + argumentPresenceItem.title)
            return
        } else {
            // Append and write back
            modes += " " + argumentPresenceItem.title
            Foundation.UserDefaults.standard.set(modes, forKey: ArgumentManagerUserDefaultsKey)
            Foundation.UserDefaults.standard.synchronize()
            print("UserDefaults: Appending argument: " + argumentPresenceItem.title)
        }
    }
    
    class func removeArgument(_ argumentPresenceItem: ArgumentPresenceItem) {
        guard var modes = Foundation.UserDefaults.standard.object(forKey: ArgumentManagerUserDefaultsKey) as? String else {
            print("UserDefaults: Argument could not be removed (nothing stored in user defaults): " + argumentPresenceItem.title)
            return
        }
        if modes.contains(argumentPresenceItem.title) {
            modes = modes.replacingOccurrences(of: argumentPresenceItem.title, with: "")
            Foundation.UserDefaults.standard.set(modes, forKey: ArgumentManagerUserDefaultsKey)
            Foundation.UserDefaults.standard.synchronize()
            print("UserDefaults: Removed argument: " + argumentPresenceItem.title)
        } else {
            print("UserDefaults: Argument could not be removed (not found): " + argumentPresenceItem.title)
        }
    }
}



